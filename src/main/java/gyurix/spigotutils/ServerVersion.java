package gyurix.spigotutils;

/**
 * Created by GyuriX on 2016.03.06..
 */
public enum ServerVersion {
    UNKNOWN, v1_7, v1_8, v1_9;
}
